package com.stepanyuk;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public enum DataForTest {

    HOST("https://sandbox-secure.unitedthinkers.com/gates/xurl"),
    USER_NAME("test_api_user"),
    PASSWORD("C8v20gAdHjig3LMRWGhm5PK1G00v08V1"),
    REQUEST_TYPE("sale"),
    //ACCOUNT_NUMBER("4111111111111111"),
    ACCOUNT_NUMBER("6011000991001201"),
    AMOUNT("5000"),
    MERCHANT_ACCOUNT_CODE("2001"),
    ACCOUNT_TYPE("R"),
    TRANSACTION_INDUSTRY_TYPE("RE"),
    HOLDER_TYPE("P"),
    HOLDER_NAME("John+Smith"),
    ACCOUNT_ACCESSORY("1017"),
    STREET("12+Main+St"),
    CITY("Denver"),
    STATE("CO"),
    ZIPCODE("30301"),
    CUSTOMER_ACCOUNT_CODE("0000000001"),
    TRANSACTION_CODE("0000000001"),
    TERMINAL_CODE("001"),
    ACCOUNT_DATA("021301000F3D0000%*6011********1201^SMITH/JOHN^*****************************?*D98ED1922DD71C873885B03FA51FBA0D72C71CE483E5CD0E5D024ECBE6F7059B590C5FF806FAAA8A71500B4011FDF9C197600E8FF0A46322DF3E33D725BEA0FE5AF418E3421F74B9BC215A971369E0D9A04B7157C1299B31A267A483629949003A0002E000E658D803");

    private String stringValue;

    DataForTest(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public String getStringValueEncode() throws UnsupportedEncodingException {
        return URLEncoder.encode(stringValue, "UTF-8");
    }
}
