package com.stepanyuk;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.Properties;

public class ServiceForTest {

    private static volatile ServiceForTest serviceTestInstance;

    private String host;
    private String user_name;
    private String password;

    private ServiceForTest() {
        Properties property = new Properties();
        try(InputStream inputStream = getClass().getClassLoader().getResourceAsStream("authorization.properties");) {
            property.load(inputStream);

            host      = property.getProperty("host");
            user_name = property.getProperty("user_name");
            password  = property.getProperty("password");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static ServiceForTest getInstance(){

        ServiceForTest local = serviceTestInstance;

        if (local == null) {
            synchronized (ServiceForTest.class){
                local = serviceTestInstance;
                if (local == null) {
                    serviceTestInstance = local = new ServiceForTest();
                }
            }
        }
        return local;
    }

    public String getSaleAccountNumberUrl(String responseCode)  {

        StringBuffer result = new StringBuffer();
        try {
            // It's url string for Sale
            result.append(host);
            result.append("?");
            result.append("requestType=").append(DataForTest.REQUEST_TYPE.getStringValueEncode());
            result.append("&userName=").append(user_name);
            result.append("&password=").append(password);
            result.append("&merchantAccountCode=").append(DataForTest.MERCHANT_ACCOUNT_CODE.getStringValueEncode());
            result.append("&accountType=").append(DataForTest.ACCOUNT_TYPE.getStringValueEncode());
            result.append("&transactionIndustryType=").append(DataForTest.TRANSACTION_INDUSTRY_TYPE.getStringValueEncode());
            result.append("&holderType=").append(DataForTest.HOLDER_TYPE.getStringValueEncode());
            result.append("&holderName=").append(DataForTest.HOLDER_NAME.getStringValueEncode());
            result.append("&accountNumber=").append(DataForTest.ACCOUNT_NUMBER.getStringValueEncode());
            result.append("&accountAccessory=").append(DataForTest.ACCOUNT_ACCESSORY.getStringValueEncode());
            result.append("&street=").append(DataForTest.STREET.getStringValueEncode());
            result.append("&city=").append(DataForTest.CITY.getStringValueEncode());
            result.append("&state=").append(DataForTest.STATE.getStringValueEncode());
            result.append("&customerAccountCode=").append(DataForTest.CUSTOMER_ACCOUNT_CODE.getStringValueEncode());
            result.append("&transactionCode=").append(DataForTest.TRANSACTION_CODE.getStringValueEncode());

            if (responseCode.equals("A01")) {
                result.append("&amount=").append(DataForTest.AMOUNT.getStringValueEncode());
                result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValueEncode());
            } else if (responseCode.equals("D03")) {
                result.append("&amount=").append("12000");
                result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValueEncode());
            } else if (responseCode.equals("00")) {
                result.append("&amount=").append(DataForTest.AMOUNT.getStringValueEncode());
                result.append("&zipCode=").append("11111");
            } else if (responseCode.equals("46")) {
                result.append("&amount=").append(DataForTest.AMOUNT.getStringValue());
                result.append("&zipCode=").append("22222");
            }  else if (responseCode.equals("M")) {
                result.append("&amount=").append(DataForTest.AMOUNT.getStringValueEncode());
                result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValueEncode());
                result.append("&csc=").append("111");
            }  else if (responseCode.equals("N")) {
                result.append("&amount=").append(DataForTest.AMOUNT.getStringValueEncode());
                result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValueEncode());
                result.append("&csc=").append("222");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    public String getSaleTrackDatarUrl(String responseCode) {

        StringBuffer result = new StringBuffer();
// It's url string for Sale width Track Data
//        result.append(DataForTest.HOST.getStringValue());
//        result.append("?");
//        result.append("requestType=").append(DataForTest.REQUEST_TYPE.getStringValueEncode() );
//        result.append("&userName=").append(DataForTest.USER_NAME.getStringValueEncode());
//        result.append("&password=").append(DataForTest.PASSWORD.getStringValueEncode());
//        result.append("&terminalCode=").append(DataForTest.TERMINAL_CODE.getStringValueEncode());
//        result.append("&merchantAccountCode=").append(DataForTest.MERCHANT_ACCOUNT_CODE.getStringValueEncode());
//        result.append("&accountType=").append(DataForTest.ACCOUNT_TYPE.getStringValueEncode());
//        result.append("&transactionIndustryType=").append(DataForTest.TRANSACTION_INDUSTRY_TYPE.getStringValueEncode());
//        result.append("&accountData=").append(DataForTest.ACCOUNT_DATA.getStringValueEncode());
//
//        result.append("&customerAccountCode=").append(DataForTest.CUSTOMER_ACCOUNT_CODE.getStringValueEncode());
//        result.append("&transactionCode=").append(DataForTest.TRANSACTION_CODE.getStringValueEncode());

        // It's url string for Sale
        try{
        result.append(host);
        result.append("?");
        result.append("requestType=").append(DataForTest.REQUEST_TYPE.getStringValueEncode());
        result.append("&userName=").append(user_name);
        result.append("&password=").append(password);
        result.append("&merchantAccountCode=").append(DataForTest.MERCHANT_ACCOUNT_CODE.getStringValueEncode());
        result.append("&accountType=").append(DataForTest.ACCOUNT_TYPE.getStringValueEncode());
        result.append("&transactionIndustryType=").append(DataForTest.TRANSACTION_INDUSTRY_TYPE.getStringValueEncode());
        result.append("&holderType=").append(DataForTest.HOLDER_TYPE.getStringValueEncode());
        result.append("&holderName=").append(DataForTest.HOLDER_NAME.getStringValueEncode());
        result.append("&accountNumber=").append("6011000991001201");
        result.append("&accountAccessory=").append(DataForTest.ACCOUNT_ACCESSORY.getStringValueEncode());
        result.append("&street=").append(DataForTest.STREET.getStringValueEncode());
        result.append("&city=").append(DataForTest.CITY.getStringValueEncode());
        result.append("&state=").append(DataForTest.STATE.getStringValueEncode());
        result.append("&customerAccountCode=").append(DataForTest.CUSTOMER_ACCOUNT_CODE.getStringValueEncode());
        result.append("&transactionCode=").append(DataForTest.TRANSACTION_CODE.getStringValueEncode());
        result.append("&accountData=").append(DataForTest.ACCOUNT_DATA.getStringValue());

        if (responseCode.equals("D05")) {
            result.append("&amount=").append("7000");
            result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValue());
        } else if (responseCode.equals("E02")) {
            result.append("&amount=").append("13000");
            result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValue());
        } else if (responseCode.equals("43")) {
            result.append("&amount=").append(DataForTest.AMOUNT.getStringValue());
            result.append("&zipCode=").append("33333");
        } else if (responseCode.equals("40")) {
            result.append("&amount=").append(DataForTest.AMOUNT.getStringValue());
            result.append("&zipCode=").append("44444");
        } else if (responseCode.equals("P")) {
            result.append("&amount=").append(DataForTest.AMOUNT.getStringValue());
            result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValue());
            result.append("&csc=").append("333");
        } else if (responseCode.equals("S")) {
            result.append("&amount=").append(DataForTest.AMOUNT.getStringValue());
            result.append("&zipCode=").append(DataForTest.ZIPCODE.getStringValue());
            result.append("&csc=").append("444");
        }
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return result.toString();
    }

    public String getResponseFromHttpUrlConnection(HttpURLConnection connection) throws IOException {

        String result = null;

        try(BufferedReader responseReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));){
            String inLine;
            StringBuffer responseBuffer = new StringBuffer();

            while ((inLine = responseReader.readLine()) != null){
                responseBuffer.append(inLine);
            }
            result = responseBuffer.toString();
        } finally {
            if (result == null) {
                result = "";
            }
        }

        return result;

    }
}
