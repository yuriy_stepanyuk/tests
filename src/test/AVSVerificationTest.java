
import com.stepanyuk.ServiceForTest;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class AVSVerificationTest{

    private static ServiceForTest serviceForTest;

    @BeforeClass
    public static void setUp(){
        serviceForTest = ServiceForTest.getInstance();
    }

    @Test
    public void saleResponseCode00() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleAccountNumberUrl("00"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("avsResponseCode=00"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCode46() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleAccountNumberUrl("46"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("avsResponseCode=46"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCode43() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleTrackDatarUrl("43"));

            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("avsResponseCode=43"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCode40() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleTrackDatarUrl("40"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("avsResponseCode=40"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
