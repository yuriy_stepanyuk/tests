import com.stepanyuk.ServiceForTest;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class CSCVerificationTest {

    private static ServiceForTest serviceForTest;

    @BeforeClass
    public static void setUp() {
        serviceForTest = ServiceForTest.getInstance();
    }


    @Test
    public void saleResponseCodeM() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleAccountNumberUrl("M"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("cscResponseCode=M"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCodeN() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleAccountNumberUrl("N"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("cscResponseCode=N"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCodeP() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleTrackDatarUrl("P"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("cscResponseCode=P"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCodeS() throws IOException {
        try {
            URL testUrl = new URL(serviceForTest.getSaleTrackDatarUrl("S"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("cscResponseCode=S"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
