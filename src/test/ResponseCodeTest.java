
import com.stepanyuk.ServiceForTest;

import com.eclipsesource.restfuse.*;
import com.eclipsesource.restfuse.annotation.Context;
import com.eclipsesource.restfuse.annotation.HttpTest;

import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.eclipsesource.restfuse.Assert.assertOk;

@RunWith(HttpJUnitRunner.class)
public class ResponseCodeTest {

    private static ServiceForTest serviceForTest;

    @Rule
    public Destination destination = new Destination(this, "https://sandbox-secure.unitedthinkers.com/gates");

    @Context
    private Response response;

    @BeforeClass
    public static void setUp(){
        serviceForTest = ServiceForTest.getInstance();
    }

    @Ignore
    @HttpTest(method = Method.GET,
            path = "xurl?requestType=sale&userName=test_api_user&password=C8v20gAdHjig3LMRWGhm5PK1G00v08V1&merchantAccountCode=2001&amount=5000&accountType=R&transactionIndustryType=RE&holderType=P&holderName=John+Smith&accountNumber=4111111111111111&accountAccessory=1017&street=12+Main+St&city=Denver&state=CO&zipCode=30301&customerAccountCode=0000000001&transactionCode=0000000001",
            type=MediaType.MULTIPART_FORM_DATA)
    public void saleResponseCodeRestfuseA01(){

        assertOk(response);
        org.junit.Assert.assertTrue(response.getBody().contains("responseCode=A01"));
        int i = 22;
    }

    @Test
    public void saleResponseCodeA01() {

        try {
            URL testUrl = new URL(serviceForTest.getSaleAccountNumberUrl("A01"));
            HttpURLConnection connection = (HttpURLConnection)testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("responseCode=A01"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCodeD03(){

        try {
            URL testUrl = new URL(serviceForTest.getSaleAccountNumberUrl("D03"));
            HttpURLConnection connection = (HttpURLConnection)testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("responseCode=D03"));

        } catch (IOException e){
            e.printStackTrace();
        }
    }

    @Test
    public void saleResponseCodeD05() throws IOException {

        try {
            URL testUrl = new URL(serviceForTest.getSaleTrackDatarUrl("D05"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("responseCode=D05"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void saleResponseCodeE02() throws IOException {

        try {
            URL testUrl = new URL(serviceForTest.getSaleTrackDatarUrl("E02"));
            HttpURLConnection connection = (HttpURLConnection) testUrl.openConnection();

            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            String responce = serviceForTest.getResponseFromHttpUrlConnection(connection);

            Assert.assertTrue(responce.contains("responseCode=E02"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
